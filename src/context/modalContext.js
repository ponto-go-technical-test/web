import React, { createContext, useCallback, useContext, useMemo, useState } from 'react'

import { Modal, Text, Button, Icon, Column } from 'components'

const CONFIRM_POINT_RECORD = 'confirmPointRecord'
const CENTRAL_MODAL = 'centralModal'

const ModalContext = createContext()
const useModal = () => useContext(ModalContext)

const ModalProvider = ({ children }) => {
  const [modalData, setModalData] = useState(null)

  const handleOpenModal = newModalData => setModalData(newModalData)

  const handleCloseModal = () => {
    if (modalData?.onClose) {
      modalData.onClose()
    }
    setModalData(null)
  }

  const handleConfirm = useCallback(() => {
    modalData?.onConfirm()
  }, [modalData])

  const isConfirmationType = useMemo(() => modalData?.type === CONFIRM_POINT_RECORD, [modalData])

  const isCentralModalType = useMemo(() => modalData?.type === CENTRAL_MODAL, [modalData])

  const modalContent = useMemo(
    () => ({
      title: modalData?.title,
      hours: modalData?.hours,
      date: modalData?.date,
      content: modalData?.content,
    }),
    [modalData]
  )

  return (
    <ModalContext.Provider value={{ handleOpenModal, handleCloseModal }}>
      {children}
      <Modal 
        isOpen={!!modalData} 
        centralModal={isCentralModalType ? true : false} 
        height={isConfirmationType ? 477 : 180} 
        left={isCentralModalType ? 180 : 0} 
      >
        <Icon
          name='close'
          onClick={() => handleCloseModal()}
          position='absolute'
          right={21.3}
          top={21.3}
        />
        <Column alignItems='center' mt={49}>
          <Text fontSize='20px' lineHeight='30px' fontWeight={700} color='#20292E'>
            {modalContent.title}
          </Text>
          {isConfirmationType && <Icon name='clock' mt={30.56} /> }
          <Text fontSize='30px' lineHeight='45px' fontWeight={700} color='#330693' mt={15.56}>
            {modalContent.hours}
          </Text>
          <Text
            fontSize='16px'
            lineHeight='24px'
            fontWeight={400}
            color='#330693'
            opacity='0.5'
            mt='5px'
          >
            {isConfirmationType ? modalContent.date : modalContent.content }
          </Text>
          {isConfirmationType ? (
            <Column>
              <Button onClick={handleConfirm} mt={20}>
                Bater ponto
              </Button>
              <Button
                onClick={() => handleCloseModal()}
                border='1px solid #330693'
                color='#330693'
                bg='#FFFFFF'
                mt={10}
              >
                Cancelar
              </Button>
            </Column>
          ) : null}
        </Column>
      </Modal>
    </ModalContext.Provider>
  )
}

export { ModalProvider, useModal }
