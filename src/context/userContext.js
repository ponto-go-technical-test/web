import { createContext, useContext, useEffect, useState } from 'react'

import {
  setAcessTokenInLocalStorage,
  getAcessTokenFromLocalStorage,
  clearAcessTokenFromLocalStorage
} from 'helpers'
import { login, getMe } from 'services/users'

const UserContext = createContext()
const useUser = () => useContext(UserContext)

const UserProvider = props => {
  const [user, setUser] = useState(null)

  useEffect(() => {
    const fetchUser = async () => {
      const token = getAcessTokenFromLocalStorage()
      try {
        if (token) {
          const user = await getMe()
          return setUser(user)
        }
      } catch (error) {
        console.log('error', error)
      }
    }

    fetchUser()
  }, [])

  const loginContext = async (data) => {
    const { accessToken, ...user } = await login(data)
    setAcessTokenInLocalStorage(accessToken)
    setUser(user)
  }

  const logout = () => {
    clearAcessTokenFromLocalStorage()
    setUser(null)
  }

  return <UserContext.Provider value={{ user, loginContext, logout }} {...props} />
}

export { useUser, UserProvider }
