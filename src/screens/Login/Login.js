import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'

import { 
  Container, 
  Column, 
  Image, 
  Input, 
  Text, 
  Logo, 
  Row, 
  Link, 
  Loader, 
  Button 
} from 'components'
import image from 'assets/image/image.jpg'
import { loginSchema } from 'helpers/yupSchemas'
import { useUser } from 'context/userContext'

const Login = () => {
  const { loginContext } = useUser()
  const { mutate: login, isError, isLoading } = useMutation(loginContext)

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({ resolver: loginSchema })

  return (
    <Container justifyContent='space-between' maxWidth={1440} maxHeight={987}>
      <Column width={585} alignItems='center' mt={79} ml={153}>
        <Image
          src={image}
          width='100%'
          atl='Imagem com dois homens e duas mulheres vestidos formalmente'
        />
        <Text fontSize='40px' lineHeight='60px' fontWeight={700} color='#330693' mt={20}>
          Bem vindo ao PontoGo
        </Text>
        <Text
          fontSize='25px'
          lineHeight='37.5px'
          fontWeight={400}
          color='#330693'
          width={381}
          textAlign='center'
          mt={10}
        >
          Aqui você fará toda gestão do seu sistema de pontos.
        </Text>
      </Column>
      <Column mt={258} mr={153}>
        <Logo />
        <Text fontSize='40px' lineHeight='60px' fontWeight={700} color='#330693' mt={30}>
          Faça login
        </Text>
        {isError && (
          <Row color='red' position='absolute' mt={163}>
            <Text fontSize="14px">E-mail e/ou senha inválido(s)</Text>
          </Row>
        )}
        <Column as='form' onSubmit={handleSubmit(login)}>
          <Input
            name='email'
            label='Email'
            placeholder='exemplo@email.com'
            type='email'
            ref='ref'
            {...register('email')}
            error={errors.email?.message}
            mt={23}
          />
          <Input
            name='password'
            label='Senha'
            placeholder='*************'
            type='password'
            ref='ref'
            {...register('password')}
            error={errors.password?.message}
            mt={20}
          />
          <Link
            to={'/regain-access/send-email'}
            label='Esqueci minha senha'
            fontSize='15px'
            fontWeight={400}
            mt={20}
            mb={20}
          />
          {isLoading ? (
            <Loader />
          ) : (
            <Button type='submit' width={400}>
              Entrar
            </Button>
          )}
        </Column>
      </Column>
    </Container>
  )
}

export default Login
