import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import { useNavigate } from 'react-router-dom'

import { 
  Container, 
  Column, 
  Image, 
  Logo, 
  Text, 
  Input, 
  Row, 
  Link, 
  Loader, 
  Button 
} from 'components'
import image from 'assets/image/image.jpg'
import { passwordResetSchema } from 'helpers/yupSchemas'
import { passwordReset as passwordResetService } from 'services/users'
import { useModal } from 'context/modalContext'

const PasswordReset = () => {
  const navigate = useNavigate()
  const { handleOpenModal } = useModal()

  const {
    mutate: passwordReset,
    isLoading,
    isError
  } = useMutation(passwordResetService, {
    onSuccess: () => {
      handleOpenModal({
        type: 'centralModal',
        title: 'Recuperar acesso',
        content: 'Nova senha cadastrada com sucesso!',
        onClose: () => navigate('/users/login')
      })
    }
  })

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({ resolver: passwordResetSchema })

  return (
    <Container justifyContent='space-between' maxWidth={1440} maxHeight={987}>
      <Column width={585} alignItems='center' mt={79} ml={153}>
        <Image
          src={image}
          width='100%'
          atl='Imagem com dois homens e duas mulheres vestidos formalmente'
        />
        <Text fontSize='40px' lineHeight='60px' fontWeight={700} color='#330693' mt={20}>
          Bem vindo ao PontoGo
        </Text>
        <Text
          fontSize='25px'
          lineHeight='37.5px'
          fontWeight={400}
          color='#330693'
          width={381}
          textAlign='center'
          mt={10}
        >
          Aqui você fará toda gestão do seu sistema de pontos.
        </Text>
      </Column>
      <Column mt={258} mr={153}>
        <Logo />
        <Text fontSize='40px' lineHeight='60px' fontWeight={700} color='#330693' mt={30}>
          Recuperar acesso
        </Text>
        {isError && (
          <Row color='red' position='absolute' mt={163}>
            <Text fontSize='14px'>E-mail informado não cadastrado</Text>
          </Row>
        )}
        <Text
          width={380}
          fontSize='16px'
          lineHeight='22px'
          fontWeight={400}
          color='#20292E'
          textAlign='center'
          mt={22}
        >
          Informe o código de verificação recebido no e-mail
        </Text>
        <Column as='form' onSubmit={handleSubmit(passwordReset)}>
          <Input
            name='token'
            label='Código de verificação'
            placeholder='Ex: ASh937gL'
            type='token'
            ref='ref'
            {...register('token')}
            error={errors.token?.message}
            mt={23}
          />
          <Input
            name='password'
            label='Nova senha'
            placeholder='*************'
            type='password'
            ref='ref'
            {...register('password')}
            error={errors.password?.message}
            mt={20}
          />
          <Link
            to={'/users/login'}
            label='Voltar ao login'
            fontSize='15px'
            fontWeight={400}
            mt={20}
            mb={20}
          />
          {isLoading ? (
            <Loader />
          ) : (
            <Button type='submit' width={400}>
              Cadastrar nova senha
            </Button>
          )}
        </Column>
      </Column>
    </Container>
  )
}

export default PasswordReset
