import { useEffect, useState } from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'

import { Container, Column, Text, SideBar, Button, Row, Loader, Pagination } from 'components'
import { useUser } from 'context/userContext'
import { showRegisteredTimeByUser, storeRegisteredTime } from 'services/registeredTimes'
import {
  currentHoursMinutes,
  formatCurrentDateDayMonthYear,
  formatTime,
  formatDateDayMonthYear,
  zeroPad
} from 'helpers'
import { useParams } from 'react-router-dom'
import { useModal } from 'context/modalContext'

const RegisteredTimesList = () => {
  const { id } = useParams()
  const queryClient = useQueryClient()

  const { user } = useUser()
  const { handleOpenModal, handleCloseModal } = useModal()

  const [itens, setItens] = useState([])
  const [itensPerPage, setItensPerPage] = useState(8)
  const [currentPage, setCurrentPage] = useState(0)

  const pages = Math.ceil(itens.length / itensPerPage)
  const startIndex = currentPage * itensPerPage
  const endIndex = startIndex + itensPerPage
  const currentItens = itens.slice(startIndex, endIndex)

  const { isFetching } = useQuery(
    ['registeredTime', id ? id : user.id],
    async () => await showRegisteredTimeByUser(id ? id : user.id),
    { onSuccess: data => setItens(data) }
  )

  const { mutate: registerTime } = useMutation(storeRegisteredTime, {
    onSuccess: () => {
      handleOpenModal({
        type: 'success',
        title: 'Registrar ponto',
        content: 'Ponto registrado com sucesso!',
      })
      queryClient.invalidateQueries('registeredTime')
    }
  })

  useEffect(() => {
    setCurrentPage(0)
  }, [itensPerPage])

  const handleTimeRegister = () => {
    handleOpenModal({
      type: 'confirmPointRecord',
      title: 'Registrar novo ponto',
      hours: currentHoursMinutes(),
      date: formatCurrentDateDayMonthYear(),
      onConfirm: () => {
        registerTime({ id: user.id, time_registered: Date.now() }), handleCloseModal()
      }
    })
  }

  return (
    <Container>
      <SideBar />
      <Column bg='#F2F2F2' width='100%' height='100vh' p='40px 0px'>
        {user.role !== 'administrator' && (
          <Button onClick={() => handleTimeRegister()} ml={30} mb={30}>
            Registrar ponto
          </Button>
        )}
        <Row>
          <Text fontSize='22px' lineHeight='33px' fontWeight={600} ml={28} letterSpacing='0.02em'>
            Colaborador
          </Text>
          <Text
            fontSize='22px'
            lineHeight='33px'
            fontWeight={600}
            left={519}
            position='absolute'
            letterSpacing='0.02em'
          >
            Data
          </Text>
          <Text
            fontSize='22px'
            lineHeight='33px'
            fontWeight={600}
            left={776}
            position='absolute'
            letterSpacing='0.02em'
          >
            Hora
          </Text>
        </Row>
        {isFetching ? (
          <Loader />
        ) : (
          currentItens?.map(({ id, time_registered, User }) => (
            <Row
              key={id}
              height={73}
              m='15px 30px 0px 30px'
              bg='#FFFFFF'
              border='1px solid #BCBFC0'
              borderRadius='5px'
              alignItems='center'
            >
              <Row width={5} height={45} bg='#8A53FF' borderRadius='30px' ml={15} />
              <Column ml={40} height={73} justifyContent='center'>
                <Text fontSize='22px' lineHeight='22px' fontWeight={700} letterSpacing='0.02em'>
                  {User.name}
                </Text>
                <Text
                  fontSize='16px'
                  lineHeight='16px'
                  fontWeight={400}
                  color='#20292E'
                  opacity='0.5'
                  letterSpacing='0.02em'
                  mt={2}
                >
                  {zeroPad(User.employee_code)}
                </Text>
              </Column>
              <Text
                fontSize='22px'
                lineHeight='33px'
                fontWeight={400}
                color='#20292E'
                opacity='0.6'
                letterSpacing='0.02em'
                left={519}
                position='absolute'
              >
                {formatDateDayMonthYear(time_registered)}
              </Text>
              <Text
                fontSize='22px'
                lineHeight='33px'
                fontWeight={400}
                color='#20292E'
                opacity='0.6'
                letterSpacing='0.02em'
                left={776}
                position='absolute'
              >
                {formatTime(time_registered)}h
              </Text>
            </Row>
          ))
        )}
        <Pagination
          pages={pages}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          ml={30}
          mt={20}
        />
      </Column>
    </Container>
  )
}

export default RegisteredTimesList
