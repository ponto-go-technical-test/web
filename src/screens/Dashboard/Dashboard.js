import { useEffect, useState } from 'react'
import { useQuery, useQueryClient } from 'react-query'

import { Container, Column, Text, SideBar, Row, Loader, Link, Pagination } from 'components'
import { showRegisteredTimeAllUsers } from 'services/registeredTimes'
import { formatDateDayMonthYear, formatTime, zeroPad } from 'helpers'

const Dashboard = () => {
  const [itens, setItens] = useState([])
  const [itensPerPage, setItensPerPage] = useState(9)
  const [currentPage, setCurrentPage] = useState(0)
  const queryClient = useQueryClient()

  const pages = Math.ceil(itens.length / itensPerPage)
  const startIndex = currentPage * itensPerPage
  const endIndex = startIndex + itensPerPage
  const currentItens = itens.slice(startIndex, endIndex)

  const { isFetching } = useQuery(
    'registeredTimes',
    showRegisteredTimeAllUsers,
    { 
      onSuccess: data => setItens(data),
      refetchOnWindowFocus: true,
      refetchInterval: 60000
    }
  )

  useEffect(() => {
    setCurrentPage(0)
  }, [itensPerPage])

  return (
    <Container>
      <SideBar />
      <Column bg='#F2F2F2' width='100%' height='100vh' p='40px 0px'>
        <Row>
          <Text fontSize='22px' lineHeight='33px' fontWeight={600} ml={28} letterSpacing='0.02em'>
            Colaborador
          </Text>
          <Text
            fontSize='22px'
            lineHeight='33px'
            fontWeight={600}
            left={519}
            position='absolute'
            letterSpacing='0.02em'
          >
            Data
          </Text>
          <Text
            fontSize='22px'
            lineHeight='33px'
            fontWeight={600}
            left={776}
            position='absolute'
            letterSpacing='0.02em'
          >
            Hora
          </Text>
        </Row>
        {isFetching ? (
          <Loader />
        ) : (
          currentItens?.map(({ id, user_id, time_registered, User }) => (
            <Row
              key={id}
              height={73}
              m='15px 30px 0px 30px'
              bg='#FFFFFF'
              border='1px solid #BCBFC0'
              borderRadius='5px'
              alignItems='center'
            >
              <Row width={5} height={45} bg='#8A53FF' borderRadius='30px' ml={15} />
              <Column ml={40} height={73} justifyContent='center'>
                <Link to={`/users/${user_id}/registered-times`} variant='notDecorated'>
                  <Text
                    fontSize='22px'
                    lineHeight='22px'
                    fontWeight={700}
                    letterSpacing='0.02em'
                    color='#20292E'
                  >
                    {User.name}
                  </Text>
                </Link>
                <Text
                  fontSize='16px'
                  lineHeight='16px'
                  fontWeight={400}
                  color='#20292E'
                  opacity='0.5'
                  letterSpacing='0.02em'
                  mt={2}
                >
                  {zeroPad(User.employee_code)}
                </Text>
              </Column>
              <Text
                fontSize='22px'
                lineHeight='33px'
                fontWeight={400}
                color='#20292E'
                opacity='0.6'
                letterSpacing='0.02em'
                left={519}
                position='absolute'
              >
                {formatDateDayMonthYear(time_registered)}
              </Text>
              <Text
                fontSize='22px'
                lineHeight='33px'
                fontWeight={400}
                color='#20292E'
                opacity='0.6'
                letterSpacing='0.02em'
                left={776}
                position='absolute'
              >
                {formatTime(time_registered)}h
              </Text>
            </Row>
          ))
        )}
        <Pagination
          pages={pages}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          ml={30}
          mt={20}
        />
      </Column>
    </Container>
  )
}

export default Dashboard
