import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import { useNavigate } from 'react-router-dom'

import { 
  Container, 
  Column, 
  Image, 
  Logo, 
  Text, 
  Input, 
  Row, 
  Link, 
  Loader, 
  Button 
} from 'components'
import image from 'assets/image/image.jpg'
import { sendEmailSchema } from 'helpers/yupSchemas'
import { sendToken as sendTokenService } from 'services/users'

const SendEmail = () => {
  const navigate = useNavigate()

  const {
    mutate: sendToken,
    isLoading,
    isError
  } = useMutation(sendTokenService, { onSuccess: () => navigate('/regain-access/password-reset') })
  
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({ resolver: sendEmailSchema })

  return (
    <Container justifyContent='space-between' maxWidth={1440} maxHeight={987}>
      <Column width={585} alignItems='center' mt={79} ml={153}>
        <Image
          src={image}
          width='100%'
          atl='Imagem com dois homens e duas mulheres vestidos formalmente'
        />
        <Text fontSize='40px' lineHeight='60px' fontWeight={700} color='#330693' mt={20}>
          Bem vindo ao PontoGo
        </Text>
        <Text
          fontSize='25px'
          lineHeight='37.5px'
          fontWeight={400}
          color='#330693'
          width={381}
          textAlign='center'
          mt={10}
        >
          Aqui você fará toda gestão do seu sistema de pontos.
        </Text>
      </Column>
      <Column mt={258} mr={153}>
        <Logo />
        <Text fontSize='40px' lineHeight='60px' fontWeight={700} color='#330693' mt={30}>
          Recuperar acesso
        </Text>
        {isError && (
          <Row color='red' position='absolute' mt={163}>
            <Text fontSize='14px'>E-mail informado não cadastrado</Text>
          </Row>
        )}
        <Text
          width={380}
          fontSize='16px'
          lineHeight='22px'
          fontWeight={400}
          color='#20292E'
          textAlign='center'
          mt={22}
        >
          Informe o e-mail cadastrado para receber o código de verificação
        </Text>
        <Column as='form' onSubmit={handleSubmit(sendToken)}>
          <Input
            name='email'
            label='Email'
            placeholder='exemplo@email.com'
            type='email'
            ref='ref'
            {...register('email')}
            error={errors.email?.message}
            mt={23}
          />
          <Link
            to={'/users/login'}
            label='Voltar ao login'
            fontSize='15px'
            fontWeight={400}
            mt={20}
            mb={20}
          />
          {isLoading ? (
            <Loader />
          ) : (
            <Button type='submit' width={400}>
              Receber código
            </Button>
          )}
        </Column>
      </Column>
    </Container>
  )
}

export default SendEmail
