import { provider } from 'providers'

export const storeRegisteredTime = async ({ id, ...data }) => {
  try {
    const response = await provider.post(`/users/${id}/registered-times`, data)
    return response.data
  } catch (error) {
    console.log(`Error ${error}`)
  }
}

export const showRegisteredTimeByUser = async id => {
  try {
    const response = await provider.get(`/users/${id}/registered-times`)
    return response.data
  } catch (error) {
    console.log(`Error ${error}`)
  }
}

export const showRegisteredTimeAllUsers = async () => {
  try {
    const response = await provider.get('/admin/users/registered-times')
    return response.data
  } catch (error) {
    console.log(`Error ${error}`)
  }
}
