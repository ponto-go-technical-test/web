import { provider } from 'providers'

export const login = async data => {
  try {
    const response = await provider.post('/users/login', data)
    return response.data
  } catch (error) {
    console.log(`Error ${error}`)
  }
}

export const getMe = async () => {
  try {
    const response = await provider.get('/me')
    return response.data
  } catch (error) {
    console.log(`Error ${error}`)
  }
}

export const sendToken = async data => 
  await provider.post('/users/forget-password', data)

export const passwordReset = async data => 
  await provider.post('/users/redefine-password', data)
