import { ACCESS_TOKEN } from 'helpers'

export const setAcessTokenInLocalStorage = token => localStorage.setItem(ACCESS_TOKEN, token)
export const getAcessTokenFromLocalStorage = () => localStorage.getItem(ACCESS_TOKEN)
export const clearAcessTokenFromLocalStorage = () => localStorage.removeItem(ACCESS_TOKEN)
