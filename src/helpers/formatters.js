import { format } from 'date-fns'

export const formatDateDayMonthYear = date => format(new Date(date), 'dd/MM/yyyy')

export const formatTime = date => format(new Date(date), 'HH:mm')

export const formatCurrentDateDayMonthYear = () => {
  const currentDate = new Date()
  const date = currentDate.getDate()
  const month = currentDate.getMonth()
  const year = currentDate.getFullYear()
  const monthDateYear = `${date}/${month + 1}/${year}`
  return monthDateYear
}

export const currentHoursMinutes = () => {
  const date = new Date()
  const currentHours = ('0' + date.getHours()).slice(-2)
  const currentMinutes = ('0' + date.getMinutes()).slice(-2)
  const hoursMinutes = `${currentHours} : ${currentMinutes}`
  return hoursMinutes
}

export const zeroPad = number => (number).toString().padStart(3, '0')
