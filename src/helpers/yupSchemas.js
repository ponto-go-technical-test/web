import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'

const yupShapeWithResolver = shape => yupResolver(yup.object().shape(shape))

export const loginSchema = yupShapeWithResolver({
  email: yup.string().email('Insira um e-mail válido').required('Insira seu e-mail'),
  password: yup.string().min(8, 'Mínimo de 8 caracteres no campo').required()
})

export const sendEmailSchema = yupShapeWithResolver({
  email: yup.string().email('Insira um e-mail válido').required('Insira seu e-mail')
})

export const passwordResetSchema = yupShapeWithResolver({
  token: yup.string().required('Insira o token'),
  password: yup.string().min(8, 'Mínimo de 8 caracteres no campo').required()
})
