import axios from 'axios'

import { getAcessTokenFromLocalStorage } from 'helpers'

const provider = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    Accept: 'application/json',
    Content: 'application/json'
  }
})

provider.interceptors.request.use(({ headers, ...config }) => {
  const token = getAcessTokenFromLocalStorage()
  return {
    ...config,
    headers: {
      ...headers,
      Authorization: token && `Bearer ${token}`
    }
  }
})

export default provider
