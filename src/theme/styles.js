const styles = {
  colors: {
    principalColor: '#330693',
    secundaryColor: '#8A53FF',
    grey: '#20292E',
    white: '#FFFFFF'
  }
}

export default styles
