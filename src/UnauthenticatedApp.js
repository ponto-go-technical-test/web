import { Routes, Route, Navigate } from 'react-router-dom'

import { Login, SendEmail, PasswordReset } from 'screens'

const UnauthenticatedApp = () => {
  return (
    <Routes>
      <Route path='/login' element={<Login />} />
      <Route path='/regain-access/send-email' element={<SendEmail />} />
      <Route path='/regain-access/password-reset' element={<PasswordReset />} />

      <Route path='*' element={<Navigate to='/login' />} />
    </Routes>
  )
}

export default UnauthenticatedApp
