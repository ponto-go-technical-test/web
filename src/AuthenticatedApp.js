import { Routes, Route, Navigate } from 'react-router-dom'
import { useUser } from 'context/userContext'

import { RegisteredTimesList, Dashboard } from 'screens'

const AuthenticatedApp = () => {
  const { user } = useUser()

  if (user.role === 'administrator') {
    return (
      <Routes>
        <Route path='/admin/dashboard' element={<Dashboard />} />
        <Route path='/users/:id/registered-times' element={<RegisteredTimesList />} />

        <Route path='*' element={<Navigate to='/admin/dashboard' />} />
      </Routes>
    )
  } else {
    return (
      <Routes>
        <Route path='/users/registered-times' element={<RegisteredTimesList />} />
        <Route path='*' element={<Navigate to='/users/registered-times' />} />
      </Routes>
    )
  }
}

export default AuthenticatedApp
