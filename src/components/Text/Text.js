import styled from 'styled-components'
import { space, layout, typography, position, color } from 'styled-system'
import propTypes from '@styled-system/prop-types'

const TextComponent = styled.p(space, layout, typography, position, color)

TextComponent.propTypes = {
  ...propTypes.space,
  ...propTypes.layout,
  ...propTypes.typography,
  ...propTypes.position,
  ...propTypes.color
}

export default TextComponent
