import styled from 'styled-components'
import { space, layout, position, border, color } from 'styled-system'
import propTypes from '@styled-system/prop-types'

const ButtonComponent = ({ children, ...props }) => <Button {...props}>{children}</Button>

const Button = styled.button`
  width: 200px;
  height: 50px;
  border-radius: 5px;
  background-color: #330693;
  color: #ffffff;
  font-size: 16px;
  font-weight: 400;
  ${position}
  ${layout}
  ${space}
  ${border}
  ${color}
`

ButtonComponent.propTypes = {
  ...propTypes.position,
  ...propTypes.layout,
  ...propTypes.space,
  ...propTypes.border,
  ...propTypes.color,
}

export default ButtonComponent
