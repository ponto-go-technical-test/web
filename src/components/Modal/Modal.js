import { createPortal } from 'react-dom'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'

import { Column } from 'components/Column'

const ModalComponent = ({ children, isOpen, centralModal, ...props }) => {
  const portalRef = document.body

  return createPortal(
    <ModalBackground isOpen={isOpen} centralModal={centralModal}>
      <Modal {...props}>{children}</Modal>
    </ModalBackground>,
    portalRef
  )
}

const ModalBackground = styled('div')(
  {
    width: '100%',
    height: '100vh',
    bottom: '0px',
    right: '0px',
    top: '0px',
    position: 'fixed',
    backgroundColor: 'rgba(32, 41, 46, 0.20)'
  },
  ({ isOpen, centralModal }) => css`
    opacity: ${isOpen ? 1 : 0};
    left: ${centralModal ? '0px' : '180px'};
    visibility: ${isOpen ? 'visible' : 'hidden'};
  `
)

const Modal = styled(Column)`
  width: 400px;
  height: 477px;
  right: 180px;
  bottom: 0px;
  left: 0px;
  top: 0px;
  margin: auto;
  position: absolute;
  background-color: #ffffff;
  border: 1px solid rgba(32, 41, 46, 0.3);
  border-radius: 5px;
`

ModalComponent.propTypes = {
  isOpen: PropTypes.bool,
  centralModal: PropTypes.bool
}

export default ModalComponent
