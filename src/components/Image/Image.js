import styled from 'styled-components'
import { layout, space } from 'styled-system'
import propTypes from '@styled-system/prop-types'
import PropTypes from 'prop-types'

const ImageComponent = ({ src, atl, ...props }) => <Image src={src} atl={atl} {...props} />

const Image = styled('img')(layout, space)

ImageComponent.propTypes = {
  src: PropTypes.string,
  atl: PropTypes.string,
  ...propTypes.space,
  ...propTypes.layout
}
export default ImageComponent
