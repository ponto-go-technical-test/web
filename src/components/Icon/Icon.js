import { useMemo } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import Logout from './Logout'
import Notepad from './Notepad'
import Close from './Close'
import Frames from './Frames'
import LeftArrow from './LeftArrow'
import RightArrow from './RightArrow'
import Clock from './Clock'

const IconComponent = ({ name, onClick, ...props }) => {
  const Icon = useMemo(() => {
    switch (name) {
    case 'logout':
      return Logout
    case 'notepad':
      return Notepad
    case 'close':
      return Close
    case 'frames':
      return Frames
    case 'leftArrow':
      return LeftArrow
    case 'rightArrow':
      return RightArrow
    case 'clock':
      return Clock        
    }
  }, [name])

  if (typeof onClick !== 'function') {
    return <Icon {...props} />
  }

  return (
    <Button onClick={onClick}>
      <Icon {...props} />
    </Button>
  )
}

const Button = styled.button`
  background: transparent;
`

IconComponent.propTypes = {
  name: PropTypes.string,
  onClick: PropTypes.func
}

export default IconComponent
