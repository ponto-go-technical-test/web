import Svg from './Svg'

const RightArrow = ({ ...props }) => {
  return (
    <Svg width={7} height={11} {...props}>
      <path 
        d='M0.5 9.25L4.25 5.5L0.5 1.75L1.25 0.25L6.5 5.5L1.25 10.75L0.5 9.25Z' 
        fill='#20292E' 
      />
    </Svg>
  )
}

export default RightArrow
