import Svg from './Svg'

const LeftArrow = ({ ...props }) => {
  return (
    <Svg width={6} height={11} {...props}>
      <path
        d='M6 1.5L2.25 5.25L6 9L5.25 10.5L4.5897e-07 5.25L5.25 -6.55671e-08L6 1.5Z'
        fill='#20292E'
      />
    </Svg>
  )
}

export default LeftArrow
