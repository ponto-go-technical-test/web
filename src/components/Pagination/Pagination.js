import { Row, Icon } from 'components'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'
import propTypes from '@styled-system/prop-types'
import { color } from 'styled-system'

const PaginationComponent = ({ pages, currentPage, setCurrentPage, ...props }) => {
  return (
    <Row {...props}>
      <Button
        onClick={() => {
          currentPage !== 0 && setCurrentPage(currentPage - 1)
        }}
        opacity='0.5'
      >
        <Icon name='leftArrow' />
      </Button>
      {Array.from(Array(pages), (item, index) => {
        return (
          <Button
            key={index}
            value={index}
            onClick={event => setCurrentPage(Number(event.target.value))}
            currentPage={currentPage}
            index={index}
          >
            {index + 1}
          </Button>
        )
      })}
      <Button
        onClick={() => {
          currentPage !== pages - 1 && setCurrentPage(currentPage + 1)
        }}
        opacity='0.5'
      >
        <Icon name='rightArrow' />
      </Button>
    </Row>
  )
}

const Button = styled.button`
  width: 30px;
  height: 30px;
  opacity: 0.5;
  font-size: 14px;
  font-weight: 400;
  margin-right: 5px;
  align-items: center;
  background-color: transparent;
  color: ${({ theme }) => theme.colors.grey};
  border: 0.5px solid ${({ theme }) => theme.colors.grey};
  ${({ currentPage, index }) => css`
    opacity: ${currentPage === index && 1};
  `}
  ${color}
  :hover {
    border: 1px solid ${({ theme }) => theme.colors.secundaryColor};
  }
`

PaginationComponent.propTypes = {
  pages: PropTypes.number,
  currentPage: PropTypes.number,
  setCurrentPage: PropTypes.func,
  ...propTypes.color
}

export default PaginationComponent
