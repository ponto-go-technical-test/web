import styled, { keyframes } from 'styled-components'

const LoaderComponent = () => <MiddleCircle />

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`
const MiddleCircle = styled.div`
  width: 45px;
  height: 45px;
  margin: auto;
  background: transparent;
  transform: scale(2);
  animation: ${rotate360} 1s linear infinite;
  border-left: 3px solid ${({ theme }) => theme.colors.principalColor};
  border-bottom: 2px solid transparent;
  border-right: 2px solid transparent;
  border-top: 2px solid transparent;
  border-radius: 50%;
`

export default LoaderComponent
