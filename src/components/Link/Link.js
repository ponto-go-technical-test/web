import { Link as RouterLink } from 'react-router-dom'
import styled from 'styled-components'
import { typography, color, space } from 'styled-system'
import PropTypes from 'prop-types'
import propTypes from '@styled-system/prop-types'

const LinkComponent = ({ to, label, children, ...props }) => (
  <Link to={to} {...props}>
    {label || children}
  </Link>
)

const Link = styled(RouterLink)`
  text-decoration: none;
  display: flex;
  color: ${({ theme }) => theme.colors.principalColor};
  ${color}
  ${space}
  ${typography}
`

LinkComponent.propTypes = {
  to: PropTypes.string,
  label: PropTypes.string,
  ...propTypes.color,
  ...propTypes.space,
  ...propTypes.typography
}

export default LinkComponent
