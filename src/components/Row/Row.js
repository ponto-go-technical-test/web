import styled from 'styled-components'
import { 
  position, 
  flexbox, 
  layout, 
  border, 
  space, 
  grid, 
  color
} from 'styled-system'
import propTypes from '@styled-system/prop-types'

const RowComponent = styled.div(
  {
    display: 'flex'
  },
  position,
  flexbox,
  layout,
  border,
  space,
  grid,
  color
)

RowComponent.propTypes = {
  ...propTypes.position,
  ...propTypes.flexbox,
  ...propTypes.layout,
  ...propTypes.border,
  ...propTypes.space,
  ...propTypes.grid,
  ...propTypes.color
}

export default RowComponent
