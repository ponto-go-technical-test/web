import { Logo, Column, Icon, Link, Row } from 'components'
import styled from 'styled-components'
import { useUser } from 'context/userContext'

const SideBarComponent = () => {
  const { user, logout } = useUser()

  return (
    <SideBar>
      <Column alignItems='center'>
        <Logo width={134} height={31} mt={40} mb={30} />
        {user.role === 'administrator' ? (
          <Itens>
            <Icon name='frames' />
            <Link to='/admin/dashboard' label='Dashboard' ml={14.14} />
          </Itens>
        ) : (
          <Itens>
            <Icon name='notepad' />
            <Link to={`/users/${user.id}/registered-times`} label='Meus registros' ml={10} />
          </Itens>
        )}

        <Row alignItems='center' position='absolute' bottom={21} left={21.92}>
          <Icon name='logout' />
          <Link
            to={'/login'}
            onClick={logout}
            label='Sair'
            color='#000000'
            fontSize={16}
            fontWeight={400}
            ml={11.5}
          />
        </Row>
      </Column>
    </SideBar>
  )
}

const SideBar = styled.div`
  width: 180px;
  height: 100vh;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
`
const Itens = styled.div`
  width: 180px;
  height: 104px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${({ theme }) => theme.colors.principalColor};
  border-left: 4px solid ${({ theme }) => theme.colors.principalColor};
  border-bottom: 1px solid rgba(0, 0, 0, 0.08);
  border-top: 1px solid rgba(0, 0, 0, 0.08);
`

export default SideBarComponent
