import styled from 'styled-components'
import { flexbox, layout, space } from 'styled-system'
import propTypes from '@styled-system/prop-types'

const ContainerComponent = ({ children, ...props }) => <Container {...props}>{children}</Container>

const Container = styled.div`
  width: 100%;
  display: flex;
  margin-left: auto;
  margin-right: auto;
  ${flexbox}
  ${layout} 
  ${space}
`

ContainerComponent.propTypes = {
  ...propTypes.flexbox,
  ...propTypes.layout,
  ...propTypes.space
}

export default ContainerComponent
