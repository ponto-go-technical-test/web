import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { space } from 'styled-system'
import propTypes from '@styled-system/prop-types'

import { Column, Text } from 'components'

const InputComponent = forwardRef(({ label, name, placeholder, error, type, ...props }, ref) => (
  <Column {...props}>
    {label && (
      <Text fontSize={20} fontWeight={400} lineHeight='30px'>
        {label}
      </Text>
    )}
    <Input name={name} placeholder={placeholder} type={type} error={error} ref={ref} />
    <Text fontSize='14px' position='absolute' mt={73} color='#FF0000'>
      {error}
    </Text>
  </Column>
))

const Input = styled.input`
  width: 400px;
  height: 45px;
  font-size: 16px;
  font-weight: 400px;
  line-height: 24px;
  padding: 10px 20px 11px 20px;
  color: ${({ theme }) => theme.colors.grey};
  background-color: #ffffff;
  border-radius: 5px;
  border: 1px solid rgba(32, 41, 46, 0.3);
  ${space}
`

InputComponent.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  register: PropTypes.func,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  type: PropTypes.string,
  ...propTypes.space
}

export default InputComponent
