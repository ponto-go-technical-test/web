import { BrowserRouter as Router } from 'react-router-dom'
import { createGlobalStyle } from 'styled-components'

import { Theme } from 'theme'
import { useUser } from 'context/userContext'
import AuthenticatedApp from 'AuthenticatedApp'
import UnauthenticatedApp from 'UnauthenticatedApp'

const GlobalStyles = createGlobalStyle`
  * {
    margin: 0;
    border: 0;
    padding: 0;
    outline: none;
    box-sizing: border-box;
    font-family: 'Poppins', sans-serif;
  }
  body {
    background-color: ${({ theme }) => theme.colors.white};
    color: ${({ theme }) => theme.colors.grey};
    transition: all 0.05s linear;
  }
  button, li {
    cursor: pointer;
  }
`

const App = () => {
  const { user } = useUser()

  return (
    <Theme>
      <GlobalStyles />
      <Router>{user ? <AuthenticatedApp /> : <UnauthenticatedApp />}</Router>
    </Theme>
  )
}

export default App
