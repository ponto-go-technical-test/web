# Ponto go web app

## Tecnologias utilizadas
React com JavaScript, Styled Components, Styled System e axios.

## Figma
As telas do projeto podem ser contradas [**aqui**](https://www.figma.com/file/TtYX3Q2vh9WDE14OgjtNlc/Teste_Fullstack_J%C3%BAnior_Brainny_Register?node-id=0%3A1&t=FNbkhGvwEP30sohj-0).

## Siga os passos abaixo para executar o projeto

1) Renomeie o arquivo da raiz do projeto chamado .env.example para .env e neste defina:

> - A porta em que a API esta rodando**

** **_Obrigatório_**

2) Inicie as dependências do projeto
```
 yarn install
```

3) Inicie o servidor
```
yarn start
``` 
## API
A API pode ser encontrada [**aqui**](https://gitlab.com/ponto-go-technical-test/api).

## Estrutura de diretórios

```
├── /src
|   ├── /components
|   ├── /context
|   ├── /helpers
|   ├── /providers
|   ├── /screens
|   |   ├── /Dashboard
|   |   ├── /Login
|   |   ├── /PasswordReset
|   |   ├── /RegisteredTimesList
|   |   ├── /SendEmail
|   ├── /services
|   ├── /theme
```